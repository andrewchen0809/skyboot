$(function(){
    init_file_upload();
    init_select2();
    init_on_off_toggle();
    init_resizable();
    init_datatable();
    init_datetimepicker();
    init_clear_input();
    init_filter_btn_outer_item_toggle();
    init_ul_toggle();
    init_runtime_toggle();
    init_checkbox_all_toggle();
    init_warning();
});
//wrarning toggle
function init_warning(){
    if($('.modal-warning').length>0){
        $('.modal-warning button.close').click(function(){
            $(this).parent().hide();
        });
    }
    if($('.modal-warning-outer .modal-warning').length>0){
        $('.modal-warning button.close').click(function(){
            $(this).parent().parent().hide();
        });
    }
}
//上傳的controller
function init_file_upload(){  
    numFiles ='';  
     $(document).on('change', ':file', function () {
        var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

    });

    $(':file').on('fileselect', function (event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
}
//更改 select樣式
function init_select2(){
    if($('.select2').length>0){
        $(".select2").select2({
            width: '100%'
        });
    }
    
}

//切換on off
function init_on_off_toggle(){
    if($(".on-off-toggle").length>0){
        $(".on-off-toggle").bootstrapSwitch();
      
    }
}

//啟動 datatable 
function init_datatable(){
    if($('#dataTable').length>0){
        var scroll_html = '<div class="table-responsive"></div>';       
        var table = $('#dataTable').DataTable({
            "columnDefs": [{ targets: 'no-sort', orderable: false }]});            
        $('.dataTable-outer  #dataTable_wrapper>.row:nth-child(2)').wrap(scroll_html);
        if($('#card-info').length>0){
            $('#card-info').insertAfter('.dataTable-outer  #dataTable_wrapper>.row:nth-child(1)');
        }
        if($('.dataTables_filter').length>0){
            var search_html = '<i id="filtersubmit" class="fas fa-search"></i>';
           $('.dataTables_filter label').append(search_html);
            init_datatable_function();
        }
        table.on( 'draw', function () {
            setTimeout(set_dataTable_th_wid,800);
        } );
        setTimeout(set_dataTable_th_wid,800);
        
    }
}
//datatable 加功能
function init_datatable_function(){
    if($('.dataTables_filter').length>0){
        if($('.function-add-left-row').length>0){
            $('.function-add-left-row').appendTo('.card-body.dataTable-outer .dataTables_wrapper>.row .col-sm-12.col-md-6:first-child');
        }
        if($('.function-add-right-row').length>0){
            $('.function-add-right-row').insertBefore('.dataTables_filter');
        }
    }
    // disable drop menu click event
    if($('.dataTable-outer .dropdown-menu').length>0){
        $('.dataTable-outer .dropdown-menu').on('click', function(event){          
            event.stopPropagation();
        });
    }
    init_disable_dropdown_menu_event('.filter-btn-outer .dropdown-menu');
}
// disable dropdown click event
function init_disable_dropdown_menu_event(id){
    if($(id).length>0){
        $(id).on('click', function(event){          
            event.stopPropagation();
        });
    }
}
//清除 input 字串
function init_clear_input(){
    $('.btn-clear-input').click(function(){      
        $(this).parent().find('input').val('');
    });
    
}
//toggle item
function init_filter_btn_outer_item_toggle(){
    if($('.filter-btn-outer .item').length>0){
        $('.filter-btn-outer .item .sub-title').click(function(){      
            $(this).parent().toggleClass('active'); 
        });  
        
        $('.filter-btn-outer .item.checkbox-list .input-group').click(function(){      
            $(this).toggleClass('active'); 
        });  
    }
    if($('.dropdown-display-layout-outer .list-item .item').length>0){
        $('.dropdown-display-layout-outer .list-item .item input').click(function(){      
            $(this).parent().parent().toggleClass('active'); 
        });  
    }

}
//ul toggle
function init_ul_toggle(){
    if($('.function-btn.ul-list .list-item').length>0){
        $('.function-btn.ul-list .list-item ul a.has-item').click(function(){
            $(this).toggleClass('active'); 
            $(this).next().toggleClass('active'); 
        });
    }
}
// 更改 dataTable th 寬度
function set_dataTable_th_wid(){
    if($('.modal-dialog').length>0){ 
        if($('#dataTable').length>0){           
             
           /*
            $('.modal-dialog .dataTable tbody tr:first-child td').each(function(){
                var id = $(this).index()+1;
                var wid = 'width:calc('+$(this).width()+'px - 0.6rem)';  
                $('.modal-dialog .dataTable thead tr th:nth-child('+id+')').attr('style',wid);  
                $('.modal-dialog .dataTable tbody tr td:nth-child('+id+')').attr('style',wid);                  
            });
           */
           
           $('.modal-dialog .dataTable thead tr th').each(function(){                   
                var id = $(this).index()+1;               
                var wid = $(this).attr('width'); 
                $(this).attr('style',''); 
                $('.modal-dialog .dataTable tbody tr td:nth-child('+id+')').attr('width',wid);                   
            });            
            
        }
        
    }
    //.tbody-add-scroll
    if($('.dataTable.update-td-wid').length>0){   
                                  
        $('.dataTable thead tr th').each(function(){
            var id = $(this).index()+1;
            var wid = $(this).attr('style');  
            $('.dataTable.update-td-wid tbody tr td:nth-child('+id+')').attr('style',wid);                   
        });        
    }

}
//啟動高度 resizable
function init_resizable(){
    if($('.modal-dialog .resizable01').length>0){
        $('.modal-dialog .resizable01').resizable({
            maxHeight: 550,
            minHeight: 320
        });
        $('.modal-dialog .resizable01').resize(function(){
            var total_hei = 793;
            var resizable01_hei = $(".modal-dialog .resizable01").height();
            var resizable02_hei = $(".modal-dialog .resizable02").height();
           $('.modal-dialog .resizable01-scroller').css('max-height',resizable01_hei-30);
           $('.modal-dialog .resizable02').height(total_hei-resizable01_hei); 
           $('.modal-dialog .app-list-scroller').css('max-height',resizable01_hei-100);
           $('.modal-dialog .modal-body .dataTable-outer .table-responsive .table.datatable tbody').css('height',resizable02_hei-170);
           setTimeout(set_dataTable_th_wid,800);
        });
    }
    
}
//啟動日期選擇
function init_datetimepicker(){
    if($('.datetimepicker-input').length>0){
        $('.datetimepicker-input').datepicker({           
        });
    }
}
//runtime toggle
function init_runtime_toggle(){
    if($('.datatable .runtime').length>0){
        $('.datatable .runtime').click(function(event){
            event.preventDefault();
            if($(this).parent().parent().hasClass('open')){
                $(this).find('i').attr('class','far fa-caret-square-down text-info');
            }else{
                $(this).find('i').attr('class','fas fa-caret-square-up text-info');
            }
           
            $(this).next().toggle();
            $(this).parent().parent().toggleClass('open');
        });        
    }
}
//checkbox toggle all
function init_checkbox_all_toggle(){
    if($('.check-all').length>0){
        $('.check-all').click(function(){
            if($(this).is(':checked')){
                $('.table.datatable tbody td input[type="checkbox"]').prop('checked',true);
            }else{
                $('.table.datatable tbody td input[type="checkbox"]').prop('checked',false);
            }
            
        });
    }
}