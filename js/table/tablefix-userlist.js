
var tablefixFunction = function () {
  // console.log("ready!121");

  $("#UserDataTable_filter").prepend(
    $('<div>').attr('class', 'card-navbar').append(
      $('<ul>').append(
        $('<li>').append(
          $('<a>').attr('href', '#').attr('data-title', 'Add/Upload').attr('data-toggle', 'modal').attr('data-target', '#MD-adduser').append(
            $('<i>').attr('class', 'fas fa-plus')
          ).append(" Add"),
          $('<a>').attr('href', '#').attr('data-title', 'Delete').attr('class', 'tip').attr('id', 'delete_btn').append(
            $('<i>').attr('class', 'far fa-trash-alt')

          )
        )
      )
    )
  );


  $("#UserDataTable_filter label:contains('Search:')").html('<input type="search" class="form-control" placeholder="" aria-controls="dataTable">');

  $('#UserDataTable_filter label').find('input').after($('<i>').attr('id', 'filtersubmit').attr('class', 'fas fa-search'));


  $("#filtersubmit").show();

  $('#UserDataTable_filter label').find('input')
    .focus(function () {
      $("#filtersubmit").hide();
      $(".clear-input").mousedown(function (e) {
        e.preventDefault();
        $(this).prev().val("");
      });
    })
    .blur(function () {
      $("#filtersubmit").show();

    });

  console.log("ready!122");

  $("#UserDataTable_wrapper").find('.row').last().attr("class", "row row-footer");


  // $(".card-header").after($("<div>").attr('id', 'card-info'));
  $("#UserDataTable_wrapper").find('div').eq(1).parent().after($("<div>").attr('id', 'card-info'));
  checkdelbtn();
  //checkmessage();
  //添加全選的動作;
  $("#user-checkall").click(function () {
    checkdelbtn();
    console.log('088');

    if ($("#user-checkall").prop("checked")) {
      console.log('086');

      $("input[name='user-checkbox']").each(function () {
        console.log('089');
        $(this).parent().parent().parent().prop('class', 'checked');
        $(this).prop("checked", true);
        checkmessage();
      });

      checkdelbtn();
    } else {
      console.log('085');
      $("input[name='user-checkbox']").each(function () {
        console.log('087');
        $("#card-info").empty().attr('class', '');
        $(this).parent().parent().parent().prop('class', '');
        $(this).prop("checked", false);
      });
      checkdelbtn();
    }
  });

  $(document).on('change', 'input[name="user-checkbox"]', function () {
    if (this.checked) {
      //this is checked now
      console.log('075');
      $(this).parent().parent().parent().prop('class', 'checked');
      checkmessage();
      checkdelbtn();
    } else {
      //this is unchecked now
      console.log('077');
      $(this).parent().parent().parent().prop('class', '');
      checkmessage();
      checkdelbtn();
    }
  });



  function checkdelbtn() {
    $('#delete_btn').hide();
    var deleteids_arr = [];
    $("input:checkbox[name='user-checkbox']:checked").each(function () {
      deleteids_arr.push($(this).val());
    });

    if (deleteids_arr.length > 0) {
      $('#delete_btn').show();
    } else {
      $('#delete_btn').hide();
    }

  }
  function checkmessage() {
    var totalchecknum = $("input[name='user-checkbox']").length;
    var checknum = $("input[name='user-checkbox']:checked").length;
    // console.log(checknum);
    if (checknum == 0) {
      $("#card-info").empty().attr('class', '');
    } else {
      $("#card-info").empty().attr('class', 'card-info').html('<h6>您已經選取 <h5><b>本頁面</b></h5>上 <h5>' + checknum + '</h5> 個群組 / 選取 <h5><b>項目</b></h5> 中總計 <h5>' + totalchecknum + '</h5> 個群組</h6>');
    }

  }





}

