$(function () {     
// console.log("ready!121");
   
$("#dataTable_filter").prepend(
    $('<div>').attr('class','card-navbar').append(
        $('<ul>').append(
          $('<li>').append(
            $('<a>').attr('href', '#').attr('data-title','Add/Upload').append(
              $('<i>').attr('class', 'fas fa-plus')
            ).append(" Add"),
              $('<a>').attr('href', '#').attr('data-title','Delete').attr('class', 'tip').append(
              $('<i>').attr('class', 'far fa-trash-alt')
                   
            ) 
          )
        )
      )
    );


$("#dataTable_filter label:contains('Search:')").html('<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable">');
 
$('#dataTable_filter label').find('input').after($('<i>').attr('id','filtersubmit').attr('class', 'fas fa-search'));
        

$("#filtersubmit").show();    

 $('#dataTable_filter label').find('input')
  .focus(function() {
    $("#filtersubmit").hide();
    $(".clear-input").mousedown(function(e) {
      e.preventDefault();
      $(this).prev().val("");
    });
  })
  .blur(function() {
   $("#filtersubmit").show();    
   
  });        
        
console.log("ready!122");
  
 
});