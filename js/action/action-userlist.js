function adduserFunction() {
    
    var getRowData = [];
    getRowData[0] = $("#user-id").val();
    getRowData[1] = $("#user-name").val();
    getRowData[2] = $("#user-account").val();
    //getRowData[3] = $("#user-password").val();
    //getRowData[4] = $("#user-repassword").val();
    getRowData[3] = $("#user-email").val();
    getRowData[4] = $("#user-role").val();
    getRowData[5] = $("#user-description").val();

    if (typeof getRowData[1] === 'undefined' || getRowData[1] === null) {
        $("#user-name").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }
    if (typeof getRowData[2] === 'undefined' || getRowData[2] === null) {
        $("#user-account").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }
    if (typeof getRowData[3] === 'undefined' || getRowData[3] === null) {
        $("#user-email").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }
    if (typeof getRowData[4] === 'undefined' || getRowData[4] === null) {
        $("#user-role").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }
    if (typeof getRowData[5] === 'undefined' || getRowData[5] === null) {
        $("#user-description").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }

    function checkPassword() {
        if (resUserPassword != resUserRePassword && resUserRePassword != "") {
            // $(".tip").show();
        } else {
            // $(".tip").hide();
            if (resUserPassword != resUserRePassword) {
                // $(".tip").show();
            } else {
                // $(".tip").hide();
            }
            if (resUserPassword != resUserRePassword) {
                //return false;
            }
        }
    }

    function WarningMessage() {
        var div = $("<div>").addClass('modal-warning'),
            i = $("<i>").addClass('fas fa-exclamation'),
            h5 = $("<h5>").text('Warning'),
            h6 = $("<h6>").addClass('txt-warning').text('You need to enter the necessary field!'),
            close = $("<button>").addClass('close').attr('type', 'button').append('<span aria-hidden="true">×</span>');

        var WarningDiv = div.append(i).append(h5).append(h6).append(close);

        $(".modal-body").find('.modal-warning').remove();
        $(".modal-body").prepend(WarningDiv);
        $("div.modal-warning button.close").click(function () {
            $("div.modal-warning").remove();
        });
    }

    //Transfer form data to ajax Data    
    let AddUserData = formDataToAjaxData($('#AddUserForm'));

    console.log('AddUserData', AddUserData);
    //Get action
    let actionUrl = $('#AddUserForm').attr('action');

    $.ajax({
        type: "POST",
        url: actionUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: AddUserData,
        success: function (result) {
            console.log('success');
            $('#MD-adduser').modal('hide');
            $('#modal-ribbon-success').modal('show');
            location.reload();
            $('#modal-ribbon-close').click(function () {
                loaduserFunction();
            });
        },
        beforeSend: function () {
            $('#loadingSVG').show();
            $('#loadingSVG').removeClass('d-none');
        },
        complete: function () {
            $('#loadingSVG').hide();
            $('#loadingSVG').addClass('d-none');
        },
        error: function (result) {
            console.log('error');
            $('#MD-adduser').modal('hide');
            $('#modal-ribbon-error').modal('show');
        }
    });


}

function loaduserFunction() {
    //console.log('dataTable');
    console.log(users);
    
      $('#UserDataTable').DataTable({
        "data": users,
        "columns": [
            {
                "data": "id",
                "render": function (data, type, row, meta) {
                    return "<span><input class='styled-checkbox'  id='user-id-" + row.id + "' name='user-checkbox' type='checkbox' value='value-" + row.id + "'><label for='user-id-" + row.id + "'></label></span>";
                }
            },
            {
                "data": "name",
                "render": function (data, type, row, meta) {
                    return "<a href='#' data-toggle='modal' datat-target='#MD-edituser' value='" + row.id + "' >" + row.name + "</a></td>";
                }
            },
            { "data": "account" },
            { "data": "email" },
            { "data": "role" },
            { "data": "modifieduser" },
            { "data": "description" }
        ]
    });
    var loadusertable = $('#UserDataTable').DataTable();
    //loadusertable.destroy();
    loadusertable.clear().rows.add(users).draw();
    tablefixFunction();

    $('#UserDataTable').on('click', 'a', function (e) {
        console.log('717');
        e.preventDefault;

        var table = $('#UserDataTable').DataTable();
        var rowNum = table.row($(this).parents('tr')).index();
        var rowData = table.row(rowNum).data();
        rowid = rowData.id;
        $("input#user-name").val(rowData.name);
        $("input#user-account").val(rowData.account).attr('disabled', 'disabled');
        $("input#user-email").val(rowData.email);
        $("select#user-role :selected").text(rowData.role);
        $("select#user-role :selected").val(rowData.role);
        $("textarea#user-description").val(rowData.description);
        var getRowData = [];
        // $('#MD-edituser').modal('hide');
        $('#MD-edituser').modal('show');
        getRowData[0] = rowid;
        //Input: name
        $("input#user-name").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowname = $(this).val();
            console.log(rowname);
            getRowData[1] = rowname;
        })
        //Input: account
        $("input#user-account").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowaccount = $(this).val();
            console.log(rowaccount);
            getRowData[2] = rowaccount;
        })
        //Input: password
        $("input#user-password").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowpassword = $(this).val();
            console.log(rowpassword);

        })
        $("input#user-repassword").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowrepassword = $(this).val();
            console.log(rowrepassword);

        })
        //Input: email
        $("input#user-email").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowemail = $(this).val();
            console.log(rowemail);
            getRowData[3] = rowemail;
        })
        //Select:user-role
        $("select#user-role").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowrole = $(this).val();
            console.log(rowrole);
            getRowData[4] = rowrole;
        })
        //Textarea
        $("textarea#user-description").on("change paste keyup", function (e) {
            e.preventDefault;
            var maxlength = $(this).attr('maxlength');
            var rowdesc = $(this).val();
            console.log(rowdesc);
            getRowData[5] = rowdesc
        });
        console.log('110');

        // console.log(getRowData);
        if (typeof getRowData[1] === 'undefined' || getRowData[1] === null) {
            getRowData[1] = rowData.name;
        }
        if (typeof getRowData[2] === 'undefined' || getRowData[2] === null) {
            getRowData[2] = rowData.account;
        }
        if (typeof getRowData[3] === 'undefined' || getRowData[3] === null) {
            getRowData[3] = rowData.email;
        }
        if (typeof getRowData[4] === 'undefined' || getRowData[4] === null) {
            getRowData[4] = rowData.role;
        }
        if (typeof getRowData[5] === 'undefined' || getRowData[5] === null) {
            getRowData[5] = rowData.description;
        }

<<<<<<< HEAD

        //userRowData = '{' + 'id:' + getRowData[0] +', name: "' + getRowData[1] + '", account: "' + getRowData[2]+ '", email: "' + getRowData[3]+ '", role: "' + getRowData[4]+ '", description: "' + getRowData[5] +'"}'
        //var json_userRowData = JSON.stringify(userRowData);
        //console.log(json_userRowData);
=======
		getRowData = formDataToAjaxData($('#EditUserForm'));

>>>>>>> 454a2cdacc554800163e97c2a99aafe5470ff4a9
        edituserFunction(getRowData);
        e.stopPropagation();
    });

    $('#delete_btn').click(function (e) {
        e.preventDefault;
        deleteuserFunction();
    });

}

function edituserFunction(result) {
    console.log('345');
    //var result = [rowid, rowname, rowaccount, rowemail, rowrole, rowdesc];

<<<<<<< HEAD
    $('button#edituserbtn').off().click(function (e) {
=======
	let actionUrl = $('#EditUserForm').attr('action');

    $('button#edituserbtn').click(function (e) {
>>>>>>> 454a2cdacc554800163e97c2a99aafe5470ff4a9
        e.preventDefault;
        $("#UserDataTable").empty();
        console.log('208');
        console.log('result',result);
        var resultid = result[0];
        console.log(resultid);
       

        var dataObject={id: result[0] ,name: result[1] ,account: result[2] ,email: result[3],role: result[4],decription : result[5]}

        console.log('EditUserData', JSON.stringify(dataObject));
        //Transfer form data to ajax Data     
        //let EdituserData = formDataToAjaxData($('#editUserForm'));
       // console.log('EdituserData',EdituserData);
        //Get action
        let actionUrl = $('#EditUserForm').attr('action');


        $.ajax({
            type: "PUT",
            url: actionUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: JSON.stringify(dataObject),
            success: function (result) {
                console.log('success');
                $('#MD-edituser').modal('hide');
                $('#modal-ribbon-success').modal('show');
                location.reload();
            },
            beforeSend: function () {
                $('#loadingSVG').show();
                $('#loadingSVG').removeClass('d-none');
            },
            complete: function () {
                $('#loadingSVG').hide();
                $('#loadingSVG').addClass('d-none');
            },
            error: function (result) {
                console.log('error');
                $('#MD-edituser').modal('hide');
                $('#modal-ribbon-error').modal('show');

            }
        });
    });
}

function deleteuserFunction() {
    // console.log('304')
    $('#MD-deleteuser').modal('show');

    var DeleteUserData = [];
    // Read all checked checkboxes
    $("input:checkbox[name='user-checkbox']:checked").each(function () {
        //  deleteids_arr.push($(this).val());
        var table = $('#UserDataTable').DataTable();
        var rowNum = table.row($(this).parents('tr')).index();
        var rowData = table.row(rowNum).data();
        DeleteUserData.push(rowData);
        console.log('rowData', rowData);
    });
    console.log('DeleteUserData', DeleteUserData);

    $('#DeleteUserTable').DataTable({
        "data": DeleteUserData,
        "columns": [
            // { "data": "id"},
            { "data": "name" },
            { "data": "account" },
            { "data": "email" },
            { "data": "role" }
        ],
        "searching": false,
        "paging": false
    });
    var deleteusertable = $('#DeleteUserTable').DataTable();
    deleteusertable.destroy();
    deleteusertable.clear().rows.add(DeleteUserData).draw();

    $("#deleteuserbtn").click(function () {
        //Transfer form data to ajax Data    
        let userData = formDataToAjaxData($('#DeleteUserForm'));

        //Get action
        let actionUrl = $('#DeleteUserForm').attr('action');

        $.ajax({
            type: "DELETE",
            url: actionUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: DeleteUserData,
            success: function (data) {
                console.log('success');
                $('#MD-deleteuser').modal('hide');
                $('#modal-ribbon-success').modal('show');
                location.reload();
            },
            beforeSend: function () {
                $('#loadingSVG').show();
                $('#loadingSVG').removeClass('d-none');
            },
            complete: function () {
                $('#loadingSVG').hide();
                $('#loadingSVG').addClass('d-none');
            },
            error: function (data) {
                console.log('error');
                $('#MD-deleteuser').modal('hide');
                $('#modal-ribbon-error').modal('show');
            }
        });


    });



}

