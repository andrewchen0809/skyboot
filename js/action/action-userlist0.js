

var loaduserFunction = function () {
    $.ajax({
        type: "GET",
        url: "datauser.json",
        dataType: "json",
        success: function (data) {
            var str = "<tr>";
            $.each(data, function (i, n) {
                var inputcheck = "<span><input class='styled-checkbox' name='user-checkbox' id='user-checkbox-" + n.id + "' type='checkbox' value='value-" + n.id + "'><label for='user-checkbox-" + n.id + "'></label></span>";
                str = str + "<td>" + inputcheck + "<a href='" + n.id + "'>" + n.name + "</a></td>";
                str = str + "<td>" + n.account + "</td>";
                str = str + "<td>" + n.email + "</td>";
                str = str + "<td>" + n.role + "</td>";
                str = str + "<td>" + n.modifieduser + "</td>";
                str = str + "<td>" + n.description + "</td>" + "</tr>";
                console.log(i);
            });
            str = str + "";
            $("tbody").append(str);
            console.log(str);
            $('td.dataTables_empty').hide();
        },
        beforeSend: function () {
            $('#loadingSVG').show();
            $('#loadingSVG').removeClass('d-none');
        },
        complete: function () {
            $('#loadingSVG').hide();
            $('#loadingSVG').addClass('d-none');
        },
        error: function (result) {
            console.log('error');
            //$('#MD-adduser').modal('hide');
            $('#modal-ribbon-error').modal('show');
        }
    });
}

function adduserFunction() {
    $("#adduserbtn").click(function (e) {
        e.preventDefault();

        console.log('AddUser');
        var resUserName = $("#user-name").val();
        if (resUserName == null || resUserName.length === 0) {
            $("#user-name").prop('class', 'form-control warning').attr("placeholder", "Required Field");
            WarningMessage();
        }

        var resUserAccount = $("#user-account").val();
        if (resUserAccount == null || resUserAccount.length === 0) {
            $("#user-account").prop('class', 'form-control warning').attr("placeholder", "Required Field");
            WarningMessage();
        }

        var resUserPassword = $("#user-password").val();
        if (resUserPassword == null || resUserPassword.length === 0) {
            $("#user-password").prop('class', 'form-control warning').attr("placeholder", "Required Field");
            WarningMessage();
        }

        var resUserConPassword = $("#user-conpassword").val();
        if (resUserConPassword == null || resUserConPassword.length === 0) {
            $("#user-conpassword").prop('class', 'form-control warning').attr("placeholder", "Required Field");
            WarningMessage();
        }

        if (resUserPassword != resUserConPassword && resUserConPassword != "")//此事件當兩個密碼不相等且第二個密碼是空的時候會顯示錯誤資訊
            $(".tip").show();
        else
            $(".tip").hide();//若兩次輸入的密碼相等且都不為空時，不顯示錯誤資訊。
        if (resUserPassword != resUserConPassword) {
            $(".tip").show();//當兩個密碼不相等時則顯示錯誤資訊
        } else {
            $(".tip").hide();
        }
        if (resUserPassword != resUserConPassword) {
            //alert("兩次輸入的密碼不一致！");
            return false;
        }

        var resUserEmail = $("#user-email").val();
        if (resUserEmail == null || resUserEmail.length === 0) {
            $("#user-email").prop('class', 'form-control warning').attr("placeholder", "Required Field");
            //console.log("sos");
        }

        // $(".modal-body").append modal-warning

        function WarningMessage() {
            var div = $("<div>").addClass('modal-warning'),
                i = $("<i>").addClass('fas fa-exclamation'),
                h5 = $("<h5>").text('Warning'),
                h6 = $("<h6>").addClass('txt-warning').text('You need to enter the necessary field!'),
                close = $("<button>").addClass('close').attr('type', 'button').append('<span aria-hidden="true">×</span>');

            var WarningDiv = div.append(i).append(h5).append(h6).append(close);

            $(".modal-body").find('.modal-warning').remove();
            $(".modal-body").prepend(WarningDiv);
        }

        $("div.modal-warning button.close").click(function () {
            $("div.modal-warning").remove();
            return false;
        });

        var resRole = $("#user-role").val();

        var resDescription = $("#user-description").val();

        var UserString = [resUserName, resUserAccount, resUserPassword, resUserConPassword, resUserEmail, resRole, resDescription];

        console.log(UserString);



        console.log('522');
        $.ajax({
            type: "POST",
            url: "/user/add/",
            data: UserString,
            success: function (result) {
                console.log('success');
                $('#MD-adduser').modal('hide');
                $('#modal-ribbon-success').modal('show');
            },
            beforeSend: function () {
                $('#loadingSVG').show();
                $('#loadingSVG').removeClass('d-none');
            },
            complete: function () {
                $('#loadingSVG').hide();
                $('#loadingSVG').addClass('d-none');
            },
            error: function (result) {
                console.log('error');
                //$('#MD-adduser').modal('hide');
                $('#modal-ribbon-error').modal('show');
            }
        });
        return false;
    });

}



