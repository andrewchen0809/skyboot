function addIPFunction() {
    var AddIPData;
    
    AddIPData = $("#inputipfield").val();
   
    if (typeof AddIPData === 'undefined' || AddIPData === null) {
        $("#limit-ip").prop('class', 'form-control warning').attr("placeholder", "Required Field");
        WarningMessage();
    }
   
    console.log('AddIPData',AddIPData);
  
    //Get action
    let actionUrl = $('#AddIPForm').attr('action');

    $.ajax({
        type: "POST",
        url: actionUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: AddIPData,
        success: function (result) {
            console.log('success');
            $('#MD-addip').modal('hide');
            $('#modal-ribbon-success').modal('show');
            location.reload();
        },
        beforeSend: function () {
            $('#loadingSVG').show();
            $('#loadingSVG').removeClass('d-none');
        },
        complete: function () {
            $('#loadingSVG').hide();
            $('#loadingSVG').addClass('d-none');
        },
        error: function (result) {
            console.log('error');
            $('#MD-addip').modal('hide');
            $('#modal-ribbon-error').modal('show');
        }
    });

}


function loadIPFunction() {
    console.log('IPdataTable');
    $('#IPdataTable').DataTable({
        "ajax": "data-ip.json",
        "columns": [
            {
                "data": "ip",
                "render": function (data, type, row, meta) {
                    return "<span><input class='styled-checkbox'  id='ip-id-" + row.id + "' name='ip-checkbox' type='checkbox' value='value-" + row.id + "'><label for='ip-id-" + row.id + "'></label></span>"+ "<a href='#' data-toggle='modal' datat-target='#MD-editip' value='" + row.id + "' >" + row.ip + "</a></td>";
                }
            },
            { "data": "createtime" }
        ]
    });
    tablefixFunction();

    $("#addipbtn").click(function (e) {
        e.preventDefault;
        console.log('952');
      //  $("#MD-addip").modal('show');
        addIPFunction();
    });


    $('#IPdataTable').on('click', 'a', function (e) {
        console.log('717');
        e.preventDefault;

        var table = $('#IPdataTable').DataTable();
        var rowNum = table.row($(this).parents('tr')).index();
        var rowData = table.row(rowNum).data();
        rowid = rowData.id;
        $("input#limit-ip").val(rowData.ip);
        
        var getRowData = [];
       
        $('#MD-editip').modal('show');
        getRowData[0] = rowid;
        //Input: name
        $("input#limit-ip").bind("keyup paste change", function (e) {
            e.preventDefault;
            let rowip = $(this).val();
            console.log(rowip);
            getRowData[1] = rowip;
        })
        

        // console.log(getRowData);
        if (typeof getRowData[1] === 'undefined' || getRowData[1] === null) {
            getRowData[1] = rowData.ip;
        }
        

        editIPFunction(getRowData);
        e.stopPropagation();
    });


    $('#delete_btn').click(function () {
        //console.log('303')
        deleteIPFunction();
    });

}

function editIPFunction(result) {
    console.log('345');
    $('button#editipbtn').off().click(function (e) {
        e.preventDefault;
        console.log('343');
        $("#IPDataTable").empty();
        console.log('208');
        console.log('result',result);
        var resultid = result[0];
        console.log(resultid);
        var dataObject={id: result[0] ,ip: result[1]}
        console.log('EditIPData', JSON.stringify(dataObject));
        let actionUrl = $('#EditIPForm').attr('action');

        $.ajax({
            type: "PUT",
            url: actionUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: JSON.stringify(dataObject),
            success: function (result) {
                console.log('success');
                $('#MD-editip').modal('hide');
                $('#modal-ribbon-success').modal('show');
                location.reload();
            },
            beforeSend: function () {
                $('#loadingSVG').show();
                $('#loadingSVG').removeClass('d-none');
            },
            complete: function () {
                $('#loadingSVG').hide();
                $('#loadingSVG').addClass('d-none');
            },
            error: function (result) {
                console.log('error');
                $('#MD-editip').modal('hide');
                $('#modal-ribbon-error').modal('show');

            }
        });
    });
}

function deleteIPFunction() {
     console.log('304')
    $('#MD-deleteip').modal('show');

    var DeleteIPData = [];
    // Read all checked checkboxes
    $("input:checkbox[name='ip-checkbox']:checked").each(function () {
        //  deleteids_arr.push($(this).val());
        var table = $('#IPdataTable').DataTable();
        var rowNum = table.row($(this).parents('tr')).index();
        var rowData = table.row(rowNum).data();
        DeleteIPData.push(rowData);
        console.log('rowData', rowData);
    });
    console.log('DeleteIPData', DeleteIPData);

    $('#DeleteIPTable').DataTable({
        "data": DeleteIPData,
        "columns": [
            // { "data": "id"},
            { "data": "ip" },
            { "data": "createtime" }
        ],
        "searching": false,
        "paging": false
    });
    var deleteiptable = $('#DeleteIPTable').DataTable();
    deleteiptable.destroy();
    deleteiptable.clear().rows.add(DeleteIPData).draw();

    $("#deleteipbtn").click(function () {
        //Transfer form data to ajax Data    
        //let ipData = formDataToAjaxData($('#DeleteIPForm'));

        //Get action
        let actionUrl = $('#DeleteIPForm').attr('action');

        $.ajax({
            type: "DELETE",
            url: actionUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: DeleteIPData,
            success: function (data) {
                console.log('success');
                $('#MD-deleteip').modal('hide');
                $('#modal-ribbon-success').modal('show');
                location.reload();
            },
            beforeSend: function () {
                $('#loadingSVG').show();
                $('#loadingSVG').removeClass('d-none');
            },
            complete: function () {
                $('#loadingSVG').hide();
                $('#loadingSVG').addClass('d-none');
            },
            error: function (data) {
                console.log('error');
                $('#MD-deleteip').modal('hide');
                $('#modal-ribbon-error').modal('show');
            }
        });


    });



}

function checkPassword() {
    if (resUserPassword != resUserConPassword && resUserConPassword != "")//此事件當兩個密碼不相等且第二個密碼是空的時候會顯示錯誤資訊
        $(".tip").show();
    else
        $(".tip").hide();//若兩次輸入的密碼相等且都不為空時，不顯示錯誤資訊。
    if (resUserPassword != resUserConPassword) {
        $(".tip").show();//當兩個密碼不相等時則顯示錯誤資訊
    } else {
        $(".tip").hide();
    }
    if (resUserPassword != resUserConPassword) {
        //alert("兩次輸入的密碼不一致！");
        return false;
    }
}

function WarningMessage() {
    var div = $("<div>").addClass('modal-warning'),
        i = $("<i>").addClass('fas fa-exclamation'),
        h5 = $("<h5>").text('Warning'),
        h6 = $("<h6>").addClass('txt-warning').text('You need to enter the necessary field!'),
        close = $("<button>").addClass('close').attr('type', 'button').append('<span aria-hidden="true">×</span>');

    var WarningDiv = div.append(i).append(h5).append(h6).append(close);

    $(".modal-body").find('.modal-warning').remove();
    $(".modal-body").prepend(WarningDiv);
    $("div.modal-warning button.close").click(function () {
        $("div.modal-warning").remove();
        return false;
    });
}
