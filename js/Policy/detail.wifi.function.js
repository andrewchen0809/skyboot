$(function () {
var WifiMode = function() {
	
     var handleEvent = function() {
     	//第一次進網頁顯示控制
     	$("select[name^='n_wifi_securityType_']").each(function(i){
            var setting_key,setting_value,ca_ndes,proxy_setting;
            setting_key		= $(this).attr("name").substring(20);
            setting_value 	= $(this).val();
            // console.log(setting_key);
            // console.log(setting_value);
            handleSecurityTypeEvent(setting_value, setting_key);
            if(setting_value == 3)
            {
                ca_ndes = $('#i_wifi_ca_ndes_'+setting_key).is(":checked");
                handleCaNdesEvent(ca_ndes, setting_key);
            }
            proxy_setting = $('#i_wifi_proxy_setting_'+setting_key).find(":selected").val();
            handleProxySettingEvent(proxy_setting, setting_key);
            securityTypeScript(setting_key);
            caNdesScript(setting_key);
            proxySettingScript(setting_key);
        });
                
        //新增
//        $("button#add_wifi_config_button").click(function (e) {
//        	var url,push_data;
//        	url 			= host_url+'ajax/policy/add/wifi/config';
//			push_data 		= {'control':policy_list.wifi_policy.wifi_configs.input_set};
//			// console.log(push_data);
//        	ajax_push(url, push_data);
//        });
        
        //Add more Wi-Fi config settings
   //     $("button.add-wifi-config-button").on('click', function (e) {
            //rate UUID then replace new setting's UUID
        //    var uuid = uuidV4();

        //    var elm = $('#d_i_wifi_config_sample').clone();
        //    elm = initAddConfig(elm, uuid);
        //    elm.css('display', 'in-line');
        //    $("#wifi_body").append(elm);

            //Regenerate bootstrap animate
        //   select2Script();
        //    $('.make-switch').bootstrapSwitch();
            
            //Scroll to new elmeent position
        //    $('html,body').animate({scrollTop:$('#i_wifi_proxy_setting_' + uuid).offset().top}, 1000);
            
            //
        //    elm.find('.policy-wifi-setting-title a').attr('onclick', 'WifiMode.deleteDev(\'' + uuid + '\')');
            
            //Add listener
        //    securityTypeScript(uuid);
        //    caNdesScript(uuid);
        //    proxySettingScript(uuid);
    //    })
        

                    
        
     }
     
     //Set add wifi-config id/name
     function initAddConfig(element, uuid){
         //Replace element root div ID         
         element.attr('id', 'd_i_wifi_config_' + uuid).attr('uuid', uuid).attr('name', 'd_n_wifi_config_' + uuid).addClass('policy-wifi-config');                  
         
         //Display
         element.show();
                  
         //Replace row id
         $.each(element.find('div.row'), function(k, v){
             $(v).attr('id', removeSuffix($(v).attr('id')) + '_' + uuid);
         });
         
         //Replace input element name and id
         $.each(element.find('input, select'), function(k, v){
             $(v).attr('id', removeSuffix($(v).attr('id')) + '_' + uuid);
             $(v).attr('name', removeSuffix($(v).attr('name')) + '_' + uuid);
         });
         
        //Remove checkbox bootstrap effect
        $.each(element.find('input[type="checkbox"]'), function(k, v){
            //Find parent bootstrap class
            var _parent = $(this).parents('.bootstrap-switch');
            //Append input field before bootstrap class
            $(_parent).before(this);
            //Remove bootstrap effect
            $(_parent).remove();
        });
        
        //Remove select bootstrap effect
        $.each(element.find('select'), function(k, v){
            //Remove bootstrap class
            $(this).next().remove();
        });
        
         return element;
     }
     
     function removeSuffix(id){

        if (typeof id !== 'undefined'){
           return id.substring(0, id.lastIndexOf('_sample'));
        }         
     }
     
     
     //處理顯示項目
     var handleSecurityTypeEvent = function(security_type, id) {         
     	var proxy_setting;
     	// console.log(security_type);
     	switch (security_type) {
			case '1':
				$('#d_i_wifi_wepKeys_'+id).show();
				$('#d_i_wifi_preSharedKey_'+id).hide();
				$('#d_i_wifi_eapMethod_'+id).hide();
				$('#d_i_wifi_phase2Method_'+id).hide();
				$('#d_i_wifi_identity_'+id).hide();
				$('#d_i_wifi_anonymousIdentity_'+id).hide();
				$('#d_i_wifi_password_'+id).hide();
				$('#d_i_wifi_ca_ndes_'+id).hide();
				$('#d_i_wifi_ca_list_'+id).hide();
				$('#d_i_wifi_client_pfx_'+id).hide();
				$('#d_i_wifi_pfx_password_'+id).hide();
				break;
			case '2':
				$('#d_i_wifi_wepKeys_'+id).hide();
				$('#d_i_wifi_preSharedKey_'+id).show();
				$('#d_i_wifi_eapMethod_'+id).hide();
				$('#d_i_wifi_phase2Method_'+id).hide();
				$('#d_i_wifi_identity_'+id).hide();
				$('#d_i_wifi_anonymousIdentity_'+id).hide();
				$('#d_i_wifi_password_'+id).hide();
				$('#d_i_wifi_ca_ndes_'+id).hide();
				$('#d_i_wifi_ca_list_'+id).hide();
				$('#d_i_wifi_client_pfx_'+id).hide();
				$('#d_i_wifi_pfx_password_'+id).hide();
				break;
			case '3':
				$('#d_i_wifi_wepKeys_'+id).hide();
				$('#d_i_wifi_preSharedKey_'+id).hide();
				$('#d_i_wifi_eapMethod_'+id).show();
				$('#d_i_wifi_phase2Method_'+id).show();
				$('#d_i_wifi_identity_'+id).show();
				$('#d_i_wifi_anonymousIdentity_'+id).show();
				$('#d_i_wifi_password_'+id).show();
				$('#d_i_wifi_ca_ndes_'+id).show();
				$('#d_i_wifi_ca_list_'+id).show();
				$('#d_i_wifi_client_pfx_'+id).show();
				$('#d_i_wifi_pfx_password_'+id).show();
				break;
			default:
				$('#d_i_wifi_wepKeys_'+id).hide();
				$('#d_i_wifi_preSharedKey_'+id).hide();
				$('#d_i_wifi_eapMethod_'+id).hide();
				$('#d_i_wifi_phase2Method_'+id).hide();
				$('#d_i_wifi_identity_'+id).hide();
				$('#d_i_wifi_anonymousIdentity_'+id).hide();
				$('#d_i_wifi_password_'+id).hide();
				$('#d_i_wifi_ca_ndes_'+id).hide();
				$('#d_i_wifi_ca_list_'+id).hide();
				$('#d_i_wifi_client_pfx_'+id).hide();
				$('#d_i_wifi_pfx_password_'+id).hide();
				break;
		}
     }

     var handleProxySettingEvent = function(proxy_setting, id) {
     	if(proxy_setting == 'directProxy')
	{
		$('#d_i_wifi_host_'+id).show();
		$('#d_i_wifi_port_'+id).show();
		$('#d_i_wifi_exclList_'+id).show();
		$('#d_i_wifi_exclList_help_'+id).show();
		$('#d_i_wifi_pacProxy_'+id).hide();
	}
	else if (proxy_setting == 'pacProxy'){
	    $('#d_i_wifi_host_' + id + 
		', #d_i_wifi_port_' + id + 
		', #d_i_wifi_exclList_' + id + 
		', #d_i_wifi_exclList_help_'+id).hide();
	   
	    $('#d_i_wifi_pacProxy_'+id).show();
	}else
	{
	    $('#d_i_wifi_host_' + id + 
		', #d_i_wifi_port_' + id +  
		', #d_i_wifi_exclList_' + id +  
		', #d_i_wifi_exclList_help_' + id +  
		', #d_i_wifi_pacProxy_' + id).hide();
	}
     }
     
     var handleCaNdesEvent = function(ca_ndes, id) {
     	if(ca_ndes)
		{
			$('#d_i_wifi_ca_list_'+id).hide();
			$('#d_i_wifi_client_pfx_'+id).hide();
			$('#d_i_wifi_pfx_password_'+id).hide();
		}
		else
		{
			$('#d_i_wifi_ca_list_'+id).show();
			$('#d_i_wifi_client_pfx_'+id).show();
			$('#d_i_wifi_pfx_password_'+id).show();
		}
     }
	 //刪除
     var handleDeleteEvent = function(id) {
     	$("#d_i_wifi_config_"+id).remove();
     }

    var ajax_push = function(url, push_data) {
            $.ajax({
                    type: 'POST',
                    url : url,
                    data: push_data,
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function(response){
                            // console.log(response);
                            if(response.status != 1)
                            {
                                    webPush($.trim(response.err_msg));
                                    return;
                            }
                            $("#wifi_body").append(response.data.html);
                    select2Script();
                    $('.make-switch').bootstrapSwitch();
                    handleSecurityTypeEvent(0,response.data.uuid);
                    handleProxySettingEvent($('#i_wifi_proxy_setting_'+response.data.uuid).find(":selected").val(), response.data.uuid);
                    securityTypeScript(response.data.uuid);
                    caNdesScript(response.data.uuid);
                    proxySettingScript(response.data.uuid);
                    $('html,body').animate({scrollTop:$('#i_wifi_proxy_setting_'+response.data.uuid).offset().top}, 1000);
                    },
                    error: function(){
                            webPush('Service Failure');
               }
       });
    }

    var select2Script = function() {
            $(".select2, .select2-multiple").select2({
            placeholder: 'Select ...',
            width: null
        });
    }

    var securityTypeScript = function(id) {
            $('#i_wifi_securityType_'+id).change(function (e) {
            handleSecurityTypeEvent($(this).val(), id);
    });
    }

    var caNdesScript = function(id) {
            $('#i_wifi_ca_ndes_'+id).on('switchChange.bootstrapSwitch', function (e, checked) {
            handleCaNdesEvent(checked, id);
        });
    }

    var proxySettingScript = function(id) {       
        $('#i_wifi_proxy_setting_'+id).change(function (e) {
            handleProxySettingEvent($(this).val(), id);
        });
    }

    return {
        init: function() {
            handleEvent();
        },
        deleteDev: function(id) {
            handleDeleteEvent(id);
        },
    };
}();

jQuery(document).ready(function() {
    WifiMode.init();
    
    //prevent wi-fi config submit
    $('#wifi_body').parents('form').submit(function (e){
        e.preventDefault();
    });
});

});